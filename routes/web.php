<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index', [
    'as' => 'trang-chu',
    'uses' => 'PageController@getIndex'
]);

Route::get('loai-san-pham/{type}', [
    'as' => 'loaisanpham',
    'uses' => 'PageController@getLoaiSp'
]);

Route::get('chi-tiet-san-pham/{id}', [
    'as' => 'chitietsanpham',
    'uses' => 'PageController@getChitiet'
]);

Route::get('lien-he', [
    'as' => 'lienhe',
    'uses' => 'PageController@getLienHe'
]);

Route::get('gioi-thieu', [
    'as' => 'gioithieu',
    'uses' => 'PageController@getGioiThieu'
]);


Route::get('dang-nhap', [
    'as' => 'login',
    'uses' => 'PageController@getLogin'
]);

Route::post('dang-nhap', [
    'as' => 'login',
    'uses' => 'PageController@postLogin'
]);

Route::get('dang-ky', [
    'as' => 'signin',
    'uses' => 'PageController@getSignin'
]);

Route::post('dang-ky', [
    'as' => 'signin',
    'uses' => 'PageController@postSignin'
]);


Route::get('dang-xuat', [
    'as' => 'logout',
    'uses' => 'PageController@getLogout'
]);


Route::get('add-to-cart/{id}', [
    'as' => 'themgiohang',
    'uses' => 'PageController@getAddtoCart'
]);

Route::get('del-cart/{id}', [
    'as' => 'xoagiohang',
    'uses' => 'PageController@getDelItemCart'
]);


Route::get('dat-hang', [
    'as' => 'dathang',
    'uses' => 'PageController@getCheckout'
]);

Route::post('dat-hang', [
    'as' => 'dathang',
    'uses' => 'PageController@postCheckout'
]);


Route::get('search', [
    'as' => 'search',
    'uses' => 'PageController@getSearch'
]);

//ROUTE CHO TRANG ADMIN

Route::get('admin', function () {
    return view('admin.theloai.list');
});

Route::group(['prefix' => 'admin', 'middleware'=>'adminLogin'], function () {
    Route::group(['prefix' => 'theloai'], function () {
        Route::get('list', 'TheLoaiController@getList');

        Route::get('edit/{id}', 'TheLoaiController@getEdit');
        Route::post('edit/{id}', 'TheLoaiController@postEdit');

        Route::get('add', 'TheLoaiController@getAdd');
        Route::post('add', 'TheLoaiController@postAdd');

        Route::get('del/{id}', 'TheLoaiController@getDelete');
    });

    Route::group(['prefix' => 'sanpham'], function () {
        Route::get('list', 'SanPhamController@getList');

        Route::get('edit/{id}', 'SanPhamController@getEdit');
        Route::post('edit/{id}', 'SanPhamController@postEdit');

        Route::get('add', 'SanPhamController@getAdd');
        Route::post('add', 'SanPhamController@postAdd');

        Route::get('del/{id}', 'SanPhamController@getDelete');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('list', 'UserController@getList');

        Route::get('dathang', 'UserController@getListDatHang');
        Route::get('delDatHang/{id}', 'UserController@getDelDatHang');

        Route::get('edit/{id}', 'UserController@getEdit');
        Route::post('edit/{id}', 'UserController@postEdit');

        Route::get('add', 'UserController@getAdd');
        Route::post('add', 'UserController@postAdd');

        Route::get('del/{id}', 'UserController@getDelete');
    });
});

Route::get('admin/login', 'UserController@getLoginAdmin');
Route::post('admin/login', 'UserController@postLoginAdmin');
Route::get('admin/logout', 'UserController@getLogoutAdmin');



//Send mail
Route::get('lienhe', 'PageController@getMail');
Route::post('/sendmail', function (\Illuminate\Http\Request $request, \Illuminate\Mail\Mailer $mailer) {
    $mailer->to($request->input('mail'))
        ->send(new \App\Mail\MyMail($request->input('title')));
    return redirect()->back();
})->name('sendmail');