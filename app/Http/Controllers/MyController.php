<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MyController extends Controller
{
    public function xinchao()
    {
        echo 'Test my controller';
    }

    public function khoahoc($ten)
    {
        echo 'Khóa học: ' . $ten;
        return redirect()->route('MyRoute');
    }

    public function GetUrl(Request $request)
    {
        if ($request->is('My*'))
            echo 'Có My';
        else
            echo 'Không có My';
    }

    public function postForm(Request $request)
    {
        echo 'Ten của bạn là:   ';
        echo $request->HoTen;
    }

//    public function setCookie(){
//        $response = new Response();
//        $response->withCookie('khoahoc', 'Laravel - phong le', 1);
//        return $response;
//    }
//
//    public function getCookie(Request $request){
//        return  $request->cookie('khoahoc');
//    }

    public function postFile(Request $request)
    {
        if ($request->hasFile('myFile')) {
            $file = $request->file('myFile');
            if ($file->getClientOriginalExtension('myFile') == 'JPEG' || 'PNG') {
                $filename = $file->getClientOriginalName('myFile');
                echo $filename;
                $file->move('img', $filename);
            }
            else{
                echo 'Khong dc phep upload file';
            }

        } else {
            echo 'chưa có file';
        }
    }

}
