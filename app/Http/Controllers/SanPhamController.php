<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\ProductType;
use App\Product;

class SanPhamController extends Controller
{
    public function getList()
    {
        $sanpham = Product::all();
        return view('admin.sanpham.list', ['sanpham' => $sanpham]);
    }

    public function getAdd()
    {
        $theloai = ProductType::all();
        return view('admin.sanpham.add', ['theloai' => $theloai]);
    }

    public function postAdd(Request $req)
    {
        $this->validate($req,
            [
                'name' => 'required|unique:products,name|min:3|max:100',
                'description' => 'required',
                'unit_price' => 'required',
                'unit' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập Tên sản phẩm',
                'name.unique' => 'Tên sản phẩm đã tồn tại',
                'name.min' => 'Tên sản phẩm phải có độ dài từ 3 đến 100 ký tự',
                'name.max' => 'Tên sản phẩm phải có độ dài từ 3 đến 100 ký tự',
                'description.required' => 'Bạn chưa nhập Mô tả',
                'unit_price.required' => 'Bạn chưa nhập Giá',
                'unit.required' => 'Bạn chưa nhập Đơn vị',
            ]);
        $sanpham = new Product();
        $sanpham->name = $req->name;
        $sanpham->id_type = $req->theloai;
        $sanpham->description = $req->description;
        $sanpham->unit_price = $req->unit_price;
        $sanpham->promotion_price = $req->promotion_price;
        $sanpham->unit = $req->unit;

        if ($req->hasFile('image')) {
            $file = $req->file('image');
            $ext = $file->getClientOriginalExtension();
            if($ext != 'jpg' && $ext != 'png' && $ext != 'jpeg') {
                return redirect('admin/sanpham/add')->with('loi', 'Bạn chỉ được  file có đuôi jpg, jpeg, png');
            }
            $name = $file->getClientOriginalName();
            $image = str_random(4) . "_" . $name;
            while (file_exists("source/image/product/" . $image)) {
                $image = str_random(4) . "_" . $name;
            }
            $file->move("source/image/product/", $image);
            $sanpham->image = $image;
        } else {
            $sanpham->image = "";
        }

        $sanpham->save();

        return redirect('admin/sanpham/add')->with('thongbao', 'Thêm Sản phẩm thành công ');
    }


    public function getEdit($id)
    {
        $theloai = ProductType::all();
        $sanpham = Product::find($id);
        return view('admin.sanpham.edit', ['sanpham' => $sanpham, 'theloai' => $theloai]);
    }

    public function postEdit(Request $req, $id)
    {
        $sanpham = Product::find($id);
        $this->validate($req,
            [
                'name' => 'required|unique:type_products,name|min:3|max:100',
                'description' => 'max:500'
            ],
            [
                'name.required' => 'Bạn chưa nhập tên sản phẩm',
                'name.unique' => 'Tên sản phẩm đã tồn tại',
                'name.min' => 'Tên sản phẩm phải có độ dài từ 3 đến 100 ký tự',
                'name.max' => 'Tênsản phẩm phải có độ dài từ 3 đến 100 ký tự',
                'description.max' => 'Mô tả có độ dài không quá 500 ký tự',
            ]);

        $sanpham->name = $req->name;
        $sanpham->id_type = $req->theloai;
        $sanpham->description = $req->description;
        $sanpham->unit_price = $req->unit_price;
        $sanpham->promotion_price = $req->promotion_price;
        $sanpham->unit = $req->unit;

        $sanpham->save();

        return redirect('admin/sanpham/edit/' . $id)->with('thongbao', 'Sửa Sản phẩm thành công');
    }

    public function getDelete($id)
    {
        $sanpham = Product::find($id);
        $sanpham->delete();

        return redirect('admin/sanpham/list')->with('thongbao', 'Xóa Sản phẩm thành công');
    }
}
