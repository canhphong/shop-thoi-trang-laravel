<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Http\Request;
use App\ProductType;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getList()
    {
        $user = User::all();
        return view('admin.user.list', ['user' => $user]);
    }

    public function getAdd()
    {
        return view('admin.user.add');
    }

    public function postAdd(Request $req)
    {
        $this->validate($req,
            [
                'name' => 'required|min:3',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:3|max:32',
                're_password' => 'required|same:password',
            ],
            [
                'name.required' => 'Bạn chưa nhập tên người dùng',
                'name.min' => 'Tên người dùng phải có ít nhất 3 ký tự',
                'email.required' => 'Bạn chưa nhập email',
                'email.unique' => 'Email đã tồn tại',
                'password.required' => 'Bạn chưa nhập mật khẩu',
                'password.min' => 'Mật khẩu phải có ít nhất 3 ký tự',
                'password.max' => 'Mật khẩu phải tối đa 32 ký tự',
                're_password.required' => 'Bạn chưa nhập lại mật khẩu',
                're_password.same' => 'Mật khẩu nhập lại chưa khớp',
            ]);
        $user = new User();
        $user->full_name = $req->name;
        $user->email = $req->email;
        $user->password = bcrypt($req->password);
        $user->password = bcrypt($req->password);
        $user->role = $req->role;
        $user->save();

        return redirect('admin/user/add')->with('thongbao', 'Thêm User thành công');
    }


    public function getEdit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', ['user' => $user]);
    }

    public function postEdit(Request $req, $id)
    {
        $user = User::find($id);
        $this->validate($req,
            [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Bạn chưa nhập tên người dùng',
                'name.min' => 'Tên người dùng phải có ít nhất 3 ký tự',
            ]);

        $user->full_name = $req->name;
        $user->role = $req->role;

        if ($req->changePassword == "on") {
            $this->validate($req,
                [
                    'password' => 'required|min:3|max:32',
                    're_password' => 'required|same:password',
                ],
                [
                    'password.required' => 'Bạn chưa nhập mật khẩu',
                    'password.min' => 'Mật khẩu phải có ít nhất 3 ký tự',
                    'password.max' => 'Mật khẩu phải tối đa 32 ký tự',
                    're_password.required' => 'Bạn chưa nhập lại mật khẩu',
                    're_password.same' => 'Mật khẩu nhập lại chưa khớp',
                ]);

            $user->password = bcrypt($req->password);
        }
        $user->save();

        return redirect('admin/user/edit/' . $id)->with('thongbao', 'Sửa User thành công');
    }

    public function getDelete($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('admin/user/list')->with('thongbao', 'Bạn đã xóa User thành công');
    }


    public function getLoginAdmin()
    {
        return view('admin.login');
    }

    public function postLoginAdmin(Request $req)
    {

        $this->validate($req,
            [
                'email' => 'required',
            ],
            [
                'email.required' => 'Bạn chưa nhập Email',
                'password.required' => 'Bạn chưa nhập mật khẩu',
                'password.max' => 'Mật khẩu không được nhỏ hơn 3 ký tự',
                'password.min' => 'Mật khẩu không được lớn hơn 32 ký tự',
            ]);

        if (Auth::attempt(['email' => $req->email, 'password' => $req->password])) {
            return redirect('admin/theloai/list');
        } else {
            return redirect('admin/login')->with('thongbao', 'Đăng nhập không thành công');
        }
    }

    public function getLogoutAdmin() {
        Auth::logout();
        return redirect('admin/login');
    }

//    Đặt hàng

    public function getListDatHang()
    {
        $dathang = Customer::all();
        return view('admin.user.dathang', ['dathang' => $dathang]);
    }

    public function getDelDatHang($id)
    {
        $dathang = Customer::find($id);
        $dathang->delete();

        return redirect('admin/user/dathang')->with('thongbao', 'Bạn đã xóa Đơn đặt hàng thành công');
    }

}
