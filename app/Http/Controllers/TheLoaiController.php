<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductType;

class TheLoaiController extends Controller
{
    public function getList()
    {
        $theloai = ProductType::all();
        return view('admin.theloai.list', ['theloai' => $theloai]);
    }

    public function getAdd()
    {
        return view('admin.theloai.add');
    }

    public function postAdd(Request $req)
    {
        $this->validate($req,
            [
                'name' => 'required|unique:type_products,name|min:3|max:100'
            ],
            [
                'name.required' => 'Bạn chưa nhập tên thể loại',
                'name.unique' => 'Tên thể loại đã tồn tại',
                'name.min' => 'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
                'name.max' => 'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
            ]);
        $theloai = new ProductType();
        $theloai->name = $req->name;
        $theloai->description = $req->description;

        if ($req->hasFile('image')) {
            $file = $req->file('image');
            $ext = $file->getClientOriginalExtension();
            if($ext != 'jpg' && $ext != 'png' && $ext != 'jpeg') {
                return redirect('admin/sanpham/add')->with('loi', 'Bạn chỉ được  file có đuôi jpg, jpeg, png');
            }
            $name = $file->getClientOriginalName();
            $image = str_random(4) . "_" . $name;
            while (file_exists("source/image/product/" . $image)) {
                $image = str_random(4) . "_" . $name;
            }
            $file->move("source/image/product/", $image);
            $theloai->image = $image;
        } else {
            $theloai->image = "";
        }
        $theloai->save();

        return redirect('admin/theloai/add')->with('thongbao', 'Thêm Thể loại thành công ');
    }


    public function getEdit($id)
    {
        $theloai = ProductType::find($id);
        return view('admin.theloai.edit', ['theloai' => $theloai]);
    }

    public function postEdit(Request $req, $id)
    {
        $theloai = ProductType::find($id);
        $this->validate($req,
            [
                'name' => 'required|unique:type_products,name|min:3|max:100',
                'description' => 'max:500'
            ],
            [
                'name.required' => 'Bạn chưa nhập tên thể loại',
                'name.unique' => 'Tên thể loại đã tồn tại',
                'name.min' => 'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
                'name.max' => 'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
                'description.max' => 'Mô tả có độ dài không quá 500 ký tự',
            ]);

        $theloai->name = $req->name;
        $theloai->description = $req->description;
        $theloai->save();

        return redirect('admin/theloai/edit/' . $id)->with('thongbao', 'Sửa Thể loại thành công');
    }

    public function getDelete($id)
    {
        $theloai = ProductType::find($id);
        $theloai->delete();

        return redirect('admin/theloai/list')->with('thongbao', 'Xóa Thể loại thành công');
    }
}
