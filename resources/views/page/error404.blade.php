<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no, minimal-ui">
    <title>Page not found | Hipmunk</title>
    <style> body {
            font-family: Helvetica, Arial, sans-serif;
            text-align: center;
        }

        h1 {
            color: #3394de;
            font-size: 120px;
            margin-top: 60px;
            margin-bottom: 20px;
        }

        h3 {
            color: limegreen;
            font-size: 50px;
            margin-top: 60px;
            margin-bottom: 20px;

        }

        p {
            color: #6e788b;
            margin: 20px auto;
            font-size: 20px;
            width: 600px;
            line-height: 24px;
        }

        a {
            color: #3394de;
            font-weight: bold;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        } </style>
</head>
<body>
<h3>Shop Thời Trang Xinh</h3>
<h1>Lỗi 404</h1>
<p>Trang này không được tìm thấy hoặc link không tồn tại !!!</p>
<img src="source/image/product/404.jpg" alt="lỗi 404">
<p><a href="{{route('trang-chu')}}">Quay về Trang chủ</a></p></body>
</html>