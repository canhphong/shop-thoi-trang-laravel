@extends('master')
@section('content')
<div class="inner-header">
    <div class="container">
        <div class="pull-left">
            <h6 class="inner-title">Giới thiệu</h6>
        </div>
        <div class="pull-right">
            <div class="beta-breadcrumb font-large">
                <a href="../../resources/views/index.html">Home</a> / <span>Giới thiệu</span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="container">
    <div id="content">
        <h2 class="text-center wow fadeInDown">Thời Trang Xinh trân trọng giới thiệu</h2>
        <div class="space20">&nbsp;</div>
        <p class="introduce-text">

            Bắt đầu hoạt động từ năm 2011, từ một cửa hàng chuyên nhập khẩu váy đầm công sở hàng Quảng Châu gốc, đến nay, Thời Trang Xinh đã có 3 chi nhánh tại Hà Nội và Tp.HCM với vị thế là một nơi sản xuất đầm thiết kế và nhập khẩu hàng Quảng Châu chất lượng và uy tín trong cả nước.<br/>

            Đặt mình vào vị trí khách hàng giữa muôn vàn website ra đời ăn theo nhờ công nghệ thông tin để chọn cho mình một nơi tin tưởng mua hàng quả là không dễ dàng gì vì chất lượng và giá cả luôn có sự chênh lệch. Vậy mua hàng ở đâu sẽ được đảm bảo về chất lượng và giá cả hợp lý nhất? Đó là câu hỏi mà chắc chắn ai mua hàng cũng đặt ra.<br/>

            Tồn tại và phát triển ngày càng vững mạnh suốt hơn 5 năm qua với số lượng khách hàng mới cũng như khách hàng thân thiết ngày càng tăng là minh chứng sống và và chắc chắn nhất cho chất lượng và niềm tin tại Thời Trang Xinh. Tại Xinh, chúng tôi LUÔN ĐẢM BẢO:<br/>

            1. Vải là yếu tố quan trọng quyết định 80% chiếc váy đẹp hay xấu. Vì vậy chọn lọc chất liệu là yếu tố chúng tôi luôn quan tâm hàng đầu. 80% là loại vải nhập có độ đàn hồi và dễ mặc, form chuẩn và thuận tiện không cần phải ủi quá nhiều. Chúng tôi luôn sản xuất từ các loại vải thời trang với giá thành tương đối không pha tạp vải may hàng chợ.<br/>

            2. Váy đầm của Thời Trang Xinh đều sản xuất tại xưởng may của thời trang xinh, chúng tôi luôn sản xuất với thái độ cẩn trọng và chắc chắn dưới dạng may gia đình: đường may luôn thẳng và dày chỉ, dây kéo và màu chỉ luôn cùng tông màu với váy đầm, không may theo dạng vắt sổ (cách may này rất nhanh nhưng mũi chỉ thường thưa không có đường nới tiết kiệm vải thường sử dụng cho hàng chợ), may mũi 1 kim và cách lai 2 cm để có đường nới cho chị em.<br/>

            3. Váy đầm nhập Quảng Châu luôn được kiểm tra mẫu mã và chất lượng trước, nếu hàng đạt chất lượng, Xinh sẽ nhập lô hàng về cho khách; nếu hàng không đạt chất lượng, Xinh sẽ không nhập hàng và không bán mẫu đó cho khách.<br/>

            4. Đặc biệt, thiết kế tại Thời Trang Xinh luôn hướng đến đối tượng khách là chị em phụ nữ độ tuổi từ 30 -50:  vải co giãn, form chuẩn dễ mặc, chiều dài váy đảm bảo theo thị hiếu của người Việt, có đủ size cho chị em lựa chọn (Hàng có size luôn vào form đẹp và chuẩn hơn hàng không size)<br/>

            5. Khoảng 5000 hàng mỗi tháng là con số nhỏ nhất của Thời Trang Xinh đối với mật độ tiêu dùng  và mức độ tin tưởng của khách hàng Việt Nam giành cho thời trang xinh.<br/>

            6 Xinh cam kết chất lượng và uy tín bằng chính sách GIAO HÀNG THU TIỀN và chính sách ĐỔI TRẢ DỄ DÀNG cho khách.<br/>

            7. Quý khách có thể đặt câu hỏi với người bán hàng để kiểm tra tính chuyên nghiệp của họ khi chào bán sản phẩm thời trang may mặc tới bạn.<br/>

            Với những tiêu chí trên chúng tôi tin chắc Thời Trang Xinh là địa điểm mua sắm đáng tin cậy cho quý khách<br/>

            Trân Trọng,<br/>
            Thời Trang Xinh
            <br/>


            Sơ đồ đến Shop Xinh: 168/25 Chế Lan Viên F Tây Thạnh Tân Phú, HCM  </p>
        <div class="space35">&nbsp;</div>

        <div class="row">
            <div class="col-sm-2 col-sm-push-2">
                <div class="beta-counter">
                    <p class="beta-counter-icon"><i class="fa fa-user"></i></p>
                    <p class="beta-counter-value timer numbers" data-to="19855" data-speed="2000">19855</p>
                    <p class="beta-counter-title">Khách hàng hài lòng</p>
                </div>
            </div>

            <div class="col-sm-2 col-sm-push-2">
                <div class="beta-counter">
                    <p class="beta-counter-icon"><i class="fa fa-picture-o"></i></p>
                    <p class="beta-counter-value timer numbers" data-to="3568" data-speed="2000">3568</p>
                    <p class="beta-counter-title">Sản phẩm</p>
                </div>
            </div>

            <div class="col-sm-2 col-sm-push-2">
                <div class="beta-counter">
                    <p class="beta-counter-icon"><i class="fa fa-clock-o"></i></p>
                    <p class="beta-counter-value timer numbers" data-to="258934" data-speed="2000">258934</p>
                    <p class="beta-counter-title">Lượt xem</p>
                </div>
            </div>

            <div class="col-sm-2 col-sm-push-2">
                <div class="beta-counter">
                    <p class="beta-counter-icon"><i class="fa fa-pencil"></i></p>
                    <p class="beta-counter-value timer numbers" data-to="150" data-speed="2000">150</p>
                    <p class="beta-counter-title">Bài viết</p>
                </div>
            </div>
        </div> <!-- .beta-counter block end -->


        </div>
    </div> <!-- #content -->
</div> <!-- .container -->
    @endsection