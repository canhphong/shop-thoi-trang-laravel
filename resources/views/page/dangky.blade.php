@extends('master')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Trang Đăng ký</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('trang-chu')}}">Trang chủ</a> / <span>Đăng ký</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="container">
        <div id="content">

            <form action="{{route('signin')}}" method="post" class="beta-form-checkout">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-sm-3"></div>
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif

                    @if(Session::has('thanhcong'))
                        <div class="alert alert-success">{{Session::get('thanhcong')}}</div>
                    @endif
                    <div class="col-sm-6">
                        <h4>Đăng ký</h4>
                        <div class="space20">&nbsp;</div>


                        <div class="form-block">
                            <label for="email">Email*</label>
                            <input type="email" name="email" placeholder="nhập email">
                        </div>

                        <div class="form-block">
                            <label for="your_last_name">Họ tên*</label>
                            <input type="text" name="fullname" placeholder="nhập họ tên">
                        </div>

                        <div class="form-block">
                            <label for="address">Địa chỉ*</label>
                            <input type="text" name="address" value="" placeholder="nhập địa chỉ">
                        </div>


                        <div class="form-block">
                            <label for="phone">Số điện thoại*</label>
                            <input type="tel" name="phone" placeholder="nhập số điện thoại">
                        </div>
                        <div class="form-block">
                            <label for="phone">Mật khẩu*</label>
                            <input type="password" name="password" placeholder="nhập mật khẩu">
                        </div>
                        <div class="form-block">
                            <label for="phone">Nhập lại mật khẩu*</label>
                            <input type="password" name="re_password" placeholder="nhập lại mật khẩu">
                        </div>
                        <div class="form-block">
                            <button type="submit" class="btn btn-primary">Đăng ký</button>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection