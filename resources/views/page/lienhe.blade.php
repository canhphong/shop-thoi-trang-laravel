@extends('master')
@section('content')

    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Bản đồ cửa hàng</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="{{route('trang-chu')}}">Trang chủ</a> / <span>Liên hệ</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">

            <div class="space50">&nbsp;</div>
            <div class="row">
                <div class="col-sm-8">
                    <h2>Liên hệ</h2>
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="space20">&nbsp;</div>
                    <p>Nếu có bất cứ vấn đề gì thắc mắc vui lòng liên hệ chúng tôi!</p>
                    <div class="space20">&nbsp;</div>
                    <form action="{{route('sendmail')}}" method="post" class="contact-form">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="form-block">
                            <input name="mail" type="email" placeholder="Email">
                        </div>
                        <div class="form-block">
                            <textarea name="title" placeholder="Nội dung"></textarea>
                        </div>
                        <div class="form-block">
                            <button type="submit" class="beta-btn primary">Gửi tin nhắn <i
                                        class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <h2>Thông tin liên hệ</h2>
                    <div class="space20">&nbsp;</div>

                    <h6 class="contact-title">Địa chỉ</h6>
                    <p>
                        168/25 Chế Lan Viên, F Tây Thạnh, Tân Phú, TP HCM
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title">Điện thoại</h6>
                    <p>
                        0122 699 1299
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title">Email</h6>
                    <p>
                        phonglecanh@gmail.com<br>
                        <a href="{{route('trang-chu')}}">www.thoitrangxinh.vn</a>
                    </p>
                </div>
            </div>
        </div> <!-- #content -->
        <div class="beta-map">
            <img src="source/image/product/map.jpg" alt="maps">
        </div>
    </div> <!-- .container -->
@endsection