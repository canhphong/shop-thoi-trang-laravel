@extends('admin.layout.index')
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Loại Sản Phẩm
                    <small class="name-text">Danh sách</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
           <div class="row">
               <div class="col-md-6">
                   @if(session('thongbao'))
                       <div class="alert alert-success">
                           {{session('thongbao')}}
                       </div>
                   @endif
               </div>
           </div>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Mô tả</th>
                    <th>Hình ảnh</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>

                @foreach($theloai as $tl)
                <tr class="odd gradeX" align="center">
                    <td>{{$tl->id}}</td>
                    <td>{{$tl->name}}<img  width="100px" src="source/image/product/{{$tl->image}}" alt=""></td>
                    <td>{{$tl->description}}</td>
                    <td>{{$tl->image}}</td>
                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/theloai/del/{{$tl->id}}"> Delete</a></td>
                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/theloai/edit/{{$tl->id}}">Edit</a></td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

