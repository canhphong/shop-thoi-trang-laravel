@extends('admin.layout.index')
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sản phẩm
                    <small>{{$sanpham->name}}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{$err}}<br/>
                        @endforeach
                    </div>
                @endif

                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <form action="admin/sanpham/edit/{{$sanpham->id}}" method="post">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>Tên sản phẩm</label>
                        <input class="form-control" name="name" placeholder="Sửa tên thể loại" required value="{{$sanpham->name}}" />
                    </div>
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" name="theloai">
                            @foreach($theloai as $tl)
                                <option value="{{$tl->id}}">{{$tl->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea class="form-control ckeditor" name="description" placeholder="Sửa mô tả" value="{{$sanpham->description}}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <input class="form-control" name="unit_price" placeholder="Sửa giá" value="{{$sanpham->unit_price}}"/>
                    </div>
                    <div class="form-group">
                        <label>Giá khuyến mãi</label>
                        <input class="form-control" name="promotion_price" placeholder="Sửa khuyến mãi" value="{{$sanpham->promotion_price}}"/>
                    </div>
                    <div class="form-group">
                        <label>Đơn vị</label>
                        <input class="form-control" name="unit" placeholder="Sửa đơn vị" value="{{$sanpham->unit}}"/>
                    </div>

                    <button type="submit" class="btn btn-primary">Sửa sản phẩm</button>
                    <button type="reset" class="btn btn-default">Sửa lại</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

