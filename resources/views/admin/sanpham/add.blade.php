@extends('admin.layout.index')
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sản Phẩm
                    <small class="name-text" >Thêm</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{$err}}<br/>
                        @endforeach
                    </div>
                @endif

                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                @if(session('loi'))
                    <div class="alert alert-danger">
                        {{session('loi')}}
                    </div>
                @endif
                <form action="admin/sanpham/add" method="post" enctype='multipart/form-data'>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>Tên sản phẩm</label>
                        <input class="form-control" name="name" placeholder="Nhập tên thể loại"/>
                    </div>
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" name="theloai">
                            @foreach($theloai as $tl)
                                <option value="{{$tl->id}}">{{$tl->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea class="form-control ckeditor" name="description" placeholder="Nhập mô tả"
                                  rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <input class="form-control" name="unit_price" placeholder="Nhập giá"/>
                    </div>
                    <div class="form-group">
                        <label>Giá khuyến mãi</label>
                        <input class="form-control" name="promotion_price" placeholder="Nhập giá khuyến mãi"/>
                    </div>
                    <div class="form-group">
                        <label>Đơn vị</label>
                        <input class="form-control" name="unit" placeholder="Nhập đơn vị"/>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                        <input type="file" class="form-control" name="image"/>
                    </div>

                    <div class="form-group">
                        <label>Hàng mới</label>
                        <label class="radio-inline">
                            <input name="hangmoi" value="0" checked="" type="radio">Không
                        </label>
                        <label class="radio-inline">
                            <input name="hangmoi" value="0" checked="" type="radio">Có
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary">Thêm thể loại</button>
                    <button type="reset" class="btn btn-default">Nhập lại</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection