@extends('admin.layout.index')
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sản phẩm
                    <small class="name-text">Danh sách</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="row">
                <div class="col-md-6">
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>Id</th>
                    <th>Tên</th>
                    <th>Mã loại</th>
                    <th>Mô tả</th>
                    <th>Giá</th>
                    <th>Giá khuyến mãi</th>
                    <th>Hình ảnh</th>
                    <th>Đơn vị</th>
                    <th>Hàng mới</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sanpham as $sp)
                    <tr class="odd gradeX" align="center">
                        <td>{{$sp->id}}</td>
                        <td>{{$sp->name}}<img width="100px" src="source/image/product/{{$sp->image}}" alt=""></td>
                        <td>{{$sp->id_type}}</td>
                        <td>{{$sp->description}}</td>
                        <td>{{$sp->unit_price}}</td>
                        <td>{{$sp->promotion_price}}</td>
                        <td>{{$sp->image}}</td>
                        <td>{{$sp->unit}}</td>
                        <td>
                            @if($sp->new == 0)
                                {{'Không'}}
                            @else
                                {{'Có'}}
                            @endif
                        </td>

                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/sanpham/del/{{$sp->id}}">
                                Delete</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/sanpham/edit/{{$sp->id}}">Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

