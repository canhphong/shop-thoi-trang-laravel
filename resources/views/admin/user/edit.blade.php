@extends('admin.layout.index')
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>{{$user->full_name}}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{$err}}<br/>
                        @endforeach
                    </div>
                @endif

                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <form action="admin/user/edit/{{$user->id}}" method="post">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>Người dùng</label>
                        <input class="form-control" name="name" placeholder="Sửa tên người dùng" required value="{{$user->full_name}}"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" name="email" readonly placeholder="Sửa email" required value="{{$user->email}}"/>
                    </div>
                    <div class="form-group">
                        <label>Đổi mật khẩu</label>
                        <input type="checkbox" id="changePassword" name="changePassword"/>
                        <input type="password" class="form-control password" disabled name="password" placeholder="Sửa mật khẩu" required"/>
                    </div>
                    <div class="form-group">
                        <label>Sửa lại mật khẩu</label>
                        <input type="password" class="form-control password" disabled name="re_password" placeholder="Sửa lại mật khẩu" required/>
                    </div>

                    <div class="form-group">
                        <label>Quyền người dùng</label>
                        <label class="radio-inline">
                            <input name="role" value="0"
                                   @if($user->role == 0)
                                           {{"checked"}}
                                           @endif
                                   checked="" type="radio">Thường
                        </label>
                        <label class="radio-inline">
                            <input name="role" value="1"
                                   @if($user->role == 1)
                                   {{"checked"}}
                                   @endif
                                   checked="" type="radio">Admin
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary">Sửa User</button>
                    <button type="reset" class="btn btn-default">Sửa lại</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#changePassword").change(function(){
                if($(this).is(":checked")){
                    $(".password").removeAttr('disabled');
                }
                else {
                    $(".password").attr('disable', '');
                }
            });
        });
    </script>
@endsection