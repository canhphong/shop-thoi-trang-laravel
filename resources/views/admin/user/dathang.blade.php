@extends('admin.layout.index')
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Đặt hàng
                    <small class="name-text">Danh sách</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="row">
                <div class="col-md-6">
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>ID Đặt hàng</th>
                    <th>Tên</th>
                    <th>Giới tính</th>
                    <th>Email</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                    <th>Nội dung</th>
                    <th>Xóa</th>
                </tr>
                </thead>
                <tbody>

                @foreach($dathang as $dh)
                    <tr class="odd gradeX" align="center">
                        <td>{{$dh->id}}</td>
                        <td>{{$dh->name}}</td>
                        <td>{{$dh->gender}}</td>
                        <td>{{$dh->email}}</td>
                        <td>{{$dh->address}}</td>
                        <td>{{$dh->phone_number}}</td>
                        <td>{{$dh->note}}</td>
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/user/delDatHang/{{$dh->id}}"> Xóa</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

