<div id="footer" class="color-div">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="widget">
                    <h4 class="widget-title">Thanh toán qua</h4>

                        <p class="pull-right pay-options">
                            <a href="{{route('trang-chu')}}"><img src="source/image/product/gh.jpg" alt=""/></a>
                        </p>

                </div>
            </div>
            <div class="col-sm-2">
                <div class="widget">
                    <h4 class="widget-title">Sản phẩm</h4>
                    <div>
                        <ul>
                            @foreach($loai_sp as $loai)
                                <li><a href="{{route('loaisanpham',$loai->id)}}">{{$loai->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-10">
                    <div class="widget">
                        <h4 class="widget-title">Liên hệ chúng tôi</h4>
                        <div>
                            <div class="contact-info">
                                <i class="fa fa-map-marker"></i>
                                <p>Địa chỉ: 168/25 Chế Lan Viên, F Tây Thạnh, Tân Phú, TP HCM</p>

                                <p>Điện thoại: 0122 699 1299</p>

                                <p>Email: phonglecanh@gmail.com</p>

                                <p>Website: www.thoitrangxinh.vn</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="widget">
                    <h4 class="widget-title">Tin khuyến mãi</h4>
                    <form action="{{route('trang-chu')}}" method="get">
                        <input type="email" name="your_email">
                        <button class="pull-right" type="submit">Nhận tin <i class="fa fa-chevron-right"></i></button>
                    </form>
                </div>
            </div>
        </div> <!-- .row -->
    </div> <!-- .container -->
</div> <!-- #footer -->
<div class="copyright">
    <div class="container">
        <p class="pull-left">&copy; Thời trang Xinh 2017</p>
        <div class="clearfix"></div>
    </div> <!-- .container -->
</div> <!-- .copyright -->